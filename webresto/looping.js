const menu = [
		{
			gambarMenu:"jpg/pancake.jpeg",
			judulMenu :"Honey Pancake",
			deskripsiMenu: "Ini adalah Honey Pancake",
			hargaMenu : "75000"
		},
		{
			gambarMenu:"jpg/strawberry smoothie.jpeg",
			judulMenu :"Strawberry Smoothie",
			deskripsiMenu: "Ini adalah strawberry smoothie",
			hargaMenu : "50000"
		},
		{
			gambarMenu:"jpg/macaron.jpeg",
			judulMenu :"Rainbow Macaron",
			deskripsiMenu: "Ini adalah Rainbow Macaron",
			hargaMenu : "25000"
		},
		{
			gambarMenu:"jpg/creamy greentea boba .jpeg",
			judulMenu :"Matcha Latte Boba",
			deskripsiMenu: "Ini adalah Matcha Latte Boba",
			hargaMenu : "50000"
		},
		{
			gambarMenu:"jpg/smoothie bowl.jpeg",
			judulMenu :"Smoothie Bowl",
			deskripsiMenu: "Ini adalah Smoothie Bowl",
			hargaMenu : "75000"
		},
		{
			gambarMenu:"jpg/chocolate pear oatmeal2.jpeg",
			judulMenu :"Choco Pear Oatmeal",
			deskripsiMenu: "Ini adalah Choco Pear Oatmeal",
			hargaMenu : "75000"
		},
		{
			gambarMenu:"jpg/Strawberry cake.jpeg",
			judulMenu :"Strawberry Cake",
			deskripsiMenu: "Ini adalah Strawberry Cake",
			hargaMenu : "85000"
		},
		{
			gambarMenu:"jpg/Lavender Latte.jpeg",
			judulMenu :"Lavender Latte Art",
			deskripsiMenu: "Ini adalah Lavender Latte Art",
			hargaMenu : "50000"
		},
		{
			gambarMenu:"jpg/lemon tea-1.jpg",
			judulMenu :"Floral Lemon Tea",
			deskripsiMenu: "Ini adalah Floral Lemon Tea",
			hargaMenu : "45000"
		},
		{
			gambarMenu:"jpg/Gluten-Free Pumpkin Spice Waffles.jpeg",
			judulMenu :"Pumpkin Spice Waffles",
			deskripsiMenu: "Ini adalah Pumpkin Spice Waffles",
			hargaMenu : "75000"
		}
];


const jumlahMENU = (array) =>{
	const jmlItemUnsur = document.querySelector('.kolom-order h3');
	const jumlahItem = array.reduce((accumulator) => {
		return accumulator + 1;
	}, 0);
	jmlItemUnsur.innerHTML = jumlahItem;
}

const callbackMap = (data, index, array)=>{
	const element = document.querySelector('.daftar-menu');

	element.innerHTML += `<div class="container" style="margin-left:150px;">
							<div class="row" style="float: left;">
								<div class="col-sm-4" >
							<div class="card m-3" style="width: 18rem;">
								<img src="${data.gambarMenu}" class"card-img-top" alt="..." style="height:200px;">
								<div class"card-body">
									<h4 class="card-title" style="text-align:center;">${data.judulMenu}</h4>
									<h5 class="card-text" style="text-align:center;">Rp.${data.hargaMenu}</h5></br>
									<p class="card-text" style="text-align:center;">${data.deskripsiMenu}</b></p><br>
									<a href="#" class="btn" style="background-color:#ca3542; color:white; margin-left:115px;margin-bottom:50px;">Buy</a><br>
										</div>
						   			</div>
								</div>
							</div>
						</div>
						`
}

menu.map(callbackMap);
jumlahMENU(menu);


const buttonElement = document.querySelector('.tombol-cari');

buttonElement.addEventListener('click',()=>{
	const hasilPencarian = menu.filter((data,index)=>{
		const inputElement = document.querySelector('.input-cari');
		const namaItem = item.judulMenu.toLowerCase();
		const keyword = inputElement.value.toLowerCase();
		return namaItem.includes(keyword);
	})

	document.querySelector('.daftar-menu').innerHTML ='';

	hasilPencarian.map(callbackMap);
	jumlahMENU(hasilPencarian);
});